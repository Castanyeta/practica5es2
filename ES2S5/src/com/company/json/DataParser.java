package com.company.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Json data importer
 */
public class DataParser {

    /** Atributs */
    JSONParser parser;
    JSONArray result;

    /** Constructors */
    public DataParser(){
        parser = new JSONParser();
    }

    /**
     * @pre true
     * @post Tota la informació de file importada en format json
     * @param file on la informació està desada
     * @throws IOException No Existeix file
     * @throws ParseException file no té un format json correcte
     */
    public void parseFile(String file) throws IOException, ParseException {
        result = (JSONArray) parser.parse(new FileReader(file));
    }

    /**
     * @pre tag existeix com etiqueta json
     * @post extret llistat d'elements de amb etiqueta tag
     * @param tag etiqueta json
     * @return el llistat d'elements extret
     */
    public List<JSONObject> getFills(String tag){
        List<JSONObject> fills = new ArrayList<>();
        for(Object obj : result){
            JSONObject jsonObj = (JSONObject) obj;
            if(jsonObj.get(tag) != null){
                for(Object fill : (JSONArray) jsonObj.get(tag))
                    fills.add((JSONObject) fill);
            }
        }
        return fills;
    }
}