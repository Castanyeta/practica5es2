package com.company.factories;
import com.company.behaviors.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Fàbrica creadora de rols
 */
public class RolFactory {

    /** Constructor */
    public RolFactory(){}

    /**
     * @pre true
     * @return Una llista amb un rol de cada.
     */
    public List<RolBehavior> createAllRols(){
        List<RolBehavior> rols = new ArrayList<>();
        rols.add(new Avancat());
        rols.add(new CentralAtacant());
        rols.add(new CentralDefensor());
        rols.add(new CentralDret());
        rols.add(new CentralEsquerra());
        rols.add(new ExteriorDretAtac());
        rols.add(new ExteriorDretDefensa());
        rols.add(new ExteriorEsquerraAtac());
        rols.add(new ExteriorEsquerraDefensa());
        rols.add(new LateralDretAtac());
        rols.add(new LateralDretDefensa());
        rols.add(new LateralEsquerraAtac());
        rols.add(new LateralEsquerraDefensa());
        rols.add(new Pivot());
        rols.add(new Porter());
        return rols;
    }

    /**
     * @pre rol és una de les possibles constants dels behaviors
     * @post Rol creat a partir de rol
     * @param rol Constant de rol per identificar-lo
     * @return El rol que correspon a rol
     */
    public RolBehavior crearRol(int rol) {
        if (rol == Avancat.AVANCAT) {
            return new Avancat();
        } else if (rol == CentralAtacant.CENTRAL_ATACANT) {
            return new CentralAtacant();
        } else if (rol==CentralDefensor.CENTRAL_DEFENSOR) {
            return  new CentralDefensor();
        } else if (rol==CentralDret.CENTRAL_DRET) {
            return  new CentralDret();
        } else if (rol==CentralEsquerra.CENTRAL_ESQUERRA){
            return  new CentralEsquerra();
        } else if (rol==ExteriorDretAtac.EXTERIOR_DRET_ATAC) {
            return  new ExteriorDretAtac();
        } else if (rol==ExteriorDretDefensa.EXTERIOR_DRET_DEFENSA) {
            return  new ExteriorDretDefensa();
        } else if (rol==ExteriorEsquerraAtac.EXTERIOR_ESQUERRA_ATAC) {
            return  new ExteriorEsquerraAtac();
        } else if (rol==ExteriorEsquerraDefensa.EXTERIOR_ESQUERRA_DEFENSA) {
            return  new ExteriorEsquerraDefensa();
        } else if (rol==LateralDretAtac.LATERAL_DRET_ATAC) {
            return  new LateralDretAtac();
        } else if (rol==LateralDretDefensa.LATERAL_DRET_DEFENSA) {
            return  new LateralDretDefensa();
        } else if (rol==LateralEsquerraAtac.LATERAL_ESQUERRA_ATAC) {
            return  new LateralEsquerraAtac();
        } else if (rol==LateralEsquerraDefensa.LATERAL_ESQUERRA_DEFENSA) {
            return  new LateralEsquerraDefensa();
        }else if (rol==Pivot.PIVOT) {
            return  new Pivot();
        }else if (rol==Porter.PORTER){
            return  new Porter();
        }else {
            return  null;
        }
    }
}
