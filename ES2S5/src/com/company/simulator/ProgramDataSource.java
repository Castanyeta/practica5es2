package com.company.simulator;

import com.company.json.DataParser;
import com.company.model.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.*;

/**
 * Aquesta classe fa de façana de factories per poder nodrir l'aplicació de les dades de qualsevol fitxer.
 */
public class ProgramDataSource {


    /** Constants */
    private final String ARBITRES_TAG = "arbitres";
    private final String EQUIPA_TAG = "equips";


    /** Atributs */
    DataParser parser; // Lector de fitxers json
    Partit partit; // Objecte a crear

    /**
     * Constructor
     */
    public ProgramDataSource(){
        parser = new DataParser();
        partit = Partit.getInstance();
    }

    /**
     * Pre : True
     * Post : Fitxer llegit i afegit a partit
     * @param file Fitxer en format JSON
     * @throws IOException No existeix el fitxer
     * @throws ParseException Json mal format
     */
    public void appendJson(String file) throws IOException, ParseException {

        parser.parseFile(file);
        List<JSONObject> arbitresParsed = parser.getFills(ARBITRES_TAG);
        for (JSONObject a : arbitresParsed) {
            Arbitre arbitre = new Arbitre(a);
            partit.addArbitre(arbitre);
        }

        List<JSONObject> equipsParser = parser.getFills(EQUIPA_TAG);
        for(JSONObject e : equipsParser){
            partit.addEquip(new Equip(e));
        }

        partit.registrarObservers();
    }

    /**
     * PRE : Fitxer afegit
     * @return si fitxer afegit retorna partit creat amb fitxer, Partit buit altrament
     */
    public Partit getPartit(){
        return partit;
    }

}