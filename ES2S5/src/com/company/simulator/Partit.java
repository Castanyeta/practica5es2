package com.company.simulator;

import com.company.UI;
import com.company.exceptions.*;
import com.company.factories.RolFactory;
import com.company.model.*;
import java.util.HashMap;
import java.util.Map;


/**
 * Singleton
 */
public class Partit {

    /** Constants */
    public static final String GROGA           = "GROGA";
    public static final String EXCLUSIO        = "EXCLUSIO";
    public static final String VERMELLA        = "VERMELLA";

    /** Instància única */
    private static Partit singleton = new Partit();

    /** Atributs */
    // Aquest simulador permet que un partit sigui jugat per molts equips, és un simulador de proves i no s'ha dissenyat per que només en juguin dos. Idem amb els àrbitres.
    private Map<Integer, Equip> equips; // Tots els equips que es vulgui testejar.
    private Map<Integer, Arbitre> arbitres; // Tots els àrbitres que es vulgui afegir
    private UI ui; // Classe que permet realitzar l'entrada i sortida d'informació

    /** Constructors */
    private Partit(){
        equips = new HashMap<>();
        arbitres = new HashMap<>();
        ui = new UI();
    }

    /** Constructor singleton */
    public static Partit getInstance(){
        return singleton;
    }

    /**
     * PRE : True
     * POST : simulador engegat
     */
    public void start(){
        int option = 0;

        ui.printList("EQUIPS" , equips.values());
        ui.printList("ARBITRES",  arbitres.values());

        do {

            ui.printTitle();

            try{
                option = ui.getMenuOption();

                if(option==1){
                    /**
                     * PASSOS QUE SEGUIM
                     * PRIMER DE TOT DEMANEM L'ID DE L'EQUIP QUE VOLEM OBTENIR, DEL QUAL OBTENIM L'ENTRENADOR
                     * DESPRES DEMANEM QUE ENS ENTRIN UNA ORDRE
                     * FEM QUE L'ENTRENADOR NOTIFIQUI L'ORDRE A TOTS ELS JUGADORS QUE ESTAN JUGANT (PASSEM EL BOOLEA "ENJOC=TRUE")
                     *
                     * */

                    String idEquip = ui.demanarString("ENTRA UN ID D'EQUIP:");

                    String ordre  = ui.demanarString("ENTRA UNA ORDRE:");
                    ordreEntrenadorPista(idEquip, ordre);
                }
                else if(option == 2){
                    String idEquip = ui.demanarString("ENTRA UN ID D'EQUIP:");
                    ui.printList("JUGADORS", equips.get(idEquip.hashCode()).getJugadors());
                    int dorsal = ui.demanarInt("ENTRA UN DORSAL:");
                    ui.printList("ARBITRES",  arbitres.values());
                    String idArbit = ui.demanarString("ENTRA UN ID D'ARBITRE");

                    sanciona(idArbit, Partit.GROGA, idEquip, dorsal);
                }
                else if(option == 3){
                    /**
                     * PASSOS QUE SEGUIM
                     * PRIMER ENTREM UN IDENTIFICADOR D'EQUIP
                     * DEMANEM EL DORSAL DEL JUGADOR
                     * DESPRES ENTREM UN IDENTIFICADOR DE ROL, EL CREEM MITJANÇANT UN FACTORY
                     * MOSTREM EL JUGADOR I EL SEU ROL "ANTIC"
                     * ASSIGNEM EL ROL AL JUGADOR
                     * MOSTREM EL JUGADOR I EL SEU ROL "NOU"
                     *
                     * */

                    String idEquip = ui.demanarString("ENTRA UN ID D'EQUIP:");
                    ui.printList("JUGADORS", equips.get(idEquip.hashCode()).getJugadors());
                    int dorsal = ui.demanarInt("ENTRA UN DORSAL:");
                    ui.printList("ROLS", new RolFactory().createAllRols());
                    int opcio = ui.demanarInt("SELECCIONA UN ROL DELS ANTERIORS (número de rol)");
                    xAgafaRolY(idEquip, dorsal, opcio);


                }else if(option == 99){

                    ui.printList("Mostrem els equips" , equips.values());
                    ui.printList("Mostrem els Àrbitres" , arbitres.values());
                }
            }
            catch (Exception ex){
                System.out.println("Entrada del programa incorrecte, torna-hi ... ");
                ex.printStackTrace();
            } catch (BadEquipException e) {
                e.printStackTrace();
            } catch (BadDorsalException e) {
                e.printStackTrace();
            }
        }while(option!=0);
    }


    /**
     * NO MODIFIER METHODS :
     * Mètodes accessibles des del package o la mateixa classe
     **************************************************************************************************/


    /**
     * PRE : True
     * POST : a afegit si no existeix.
     * @param a per afegir al map d'àrbitres
     */
    void addArbitre(Arbitre a){
        if(!arbitres.containsKey(a.hashCode()))
            arbitres.put(a.hashCode(), a);
    }

    /**
     * PRE : True
     * POST : equip afegit si no exiteix
     * @param equip a afegir al map d'equips.
     */
    void addEquip(Equip equip){
        if(!equips.containsKey(equip.hashCode()))
            equips.put(equip.hashCode(), equip);
    }

    /**
     * PRE : Equips afegits
     * POST : Afegits observers d'entrenador.
     */
    void registrarObservers(){
        for(Equip equip : equips.values()){
            Entrenador entrenador = equip.getEntrenador();

            for(Jugador j : equip.getJugadors()){
                entrenador.registerObserver(j, j.isEnJoc());
                j.setEntrenador(entrenador);
            }
        }
    }

    /**
     * PRIVATE METHODS :
     * Mètodes accessibles des de la mateixa classe
     **************************************************************************************************/

    /**
     * PRE : True
     * POST : ordre enviada a tots els jugadors de pista
     * @param idEquip id de l'equip a que realitzarà l'acció
     * @param ordre missatge que envia l'entrenador als jugadors
     * @throws BadEquipException Quan l'id de l'equip no existeix
     */
    private void ordreEntrenadorPista(String idEquip, String ordre) throws BadEquipException {
        if(!equips.containsKey(idEquip.hashCode())){
            throw  new BadEquipException("No existeix un equip amb id " + idEquip);
        }

        Equip equip = equips.get(idEquip.hashCode());
        equip.entrenadorOrdena(ordre, true);

        ui.printList("Qui ha rebut l'ordre : ", equip.getPista());
    }

    /**
     * PRE : True
     * POST : colegiat amonesta amb tipus a dorsal d'equip
     * @param colegiat identificador de l'àrbitre
     * @param tipus de sanció que realitzarà
     * @param idEquip identificador de l'equip
     * @param dorsal identificador del jugador donat un equip.
     * @throws BadArbitreException Àrbitre no existeix
     * @throws BadSancioException Sanció no existeix
     * @throws BadEquipException Equip no existeix
     * @throws BadDorsalException Dorsl no existeix a equip
     */
    private void sanciona(String colegiat, String tipus, String idEquip, int dorsal) throws BadArbitreException, BadSancioException, BadEquipException, BadDorsalException {
        if(!arbitres.containsKey(colegiat.hashCode()))
            throw new BadArbitreException("No existeix l'arbitre amb num de colegiat : " + colegiat);

        Arbitre arbitre = arbitres.get(colegiat.hashCode());
        MembreDeClub s = seleccionaMembre(idEquip, dorsal);
        arbitre.sanciona(tipus, s);
        if(s.isExpulsat()){
            Equip e = equips.get(idEquip.hashCode());
            e.getEntrenador().removeObserver(s);
        }
        ui.printElement("El sancionat ha quedat així (retratado): ", s);
    }


    /**
     * PRE : True
     * POST : jugador amb dorsal d'isEquip té rol rol
     * @param idEquip identificador de l'equip
     * @param dorsal identificador del jugador donat un equip
     * @param rol identificador del rol (Constant de RolFactory)
     * @throws BadEquipException L'equip no existeix
     * @throws BadDorsalException El dorsal no existeix a l'idEquip
     */
    private void xAgafaRolY(String idEquip, int dorsal, int rol) throws BadEquipException, BadDorsalException {
        Jugador j = seleccionaMembre(idEquip, dorsal);
        ui.printElement("Jugador abans de canviar-li el rol", j);
        j.setRol(new RolFactory().crearRol(rol));
        ui.printElement("Jugador despres de canviar-li el rol",j);
    }

    /**
     * PRE : True
     * POST : s'ha retornat jugador amb dorsal d'idEquip
     * @param idEquip Identificador de l'equip
     * @param dorsal Identificador de jugador donat un equip
     * @return Jugador amb dorsal d'idEquip
     * @throws BadDorsalException El dorsal no existeix
     * @throws BadEquipException L'equip no existeix
     */
    private Jugador seleccionaMembre(String idEquip, int dorsal) throws BadDorsalException, BadEquipException {
        if(!equips.containsKey(idEquip.hashCode()))
            throw  new BadEquipException("No existeix un equip amb id " + idEquip);

        Equip equip = equips.get(idEquip.hashCode());

        if(!equip.conteJugador(dorsal))
            throw new BadDorsalException("No existeix el dorsal : " + dorsal);

        return equip.getJugador(dorsal);
    }
}
