package com.company;
import java.util.Collection;
import java.util.Scanner;

public class UI {


    public UI(){}

    public int getMenuOption() {
        printMenu();
        return demanarInt("ENTRA UNA OPCIO:");
    }

    public int demanarInt(String prompt){
        Scanner scanner = new Scanner(System.in);
        System.out.println(prompt);
        return scanner.nextInt();
    }

    public String demanarString(String prompt){
        Scanner scanner = new Scanner(System.in);
        System.out.println(prompt);
        return scanner.nextLine();
    }

    public void printElement(String title, Object o){
        System.out.println("");
        System.out.println(title + " : ");
        System.out.println(o);
    }

    public void printList(String title, Collection printables){
        System.out.println("**************************************************************************************************************************\n" + title);
        for(Object obj : printables){
            System.out.println(obj);
        }
    }



    public void printTitle(){
        System.out.println("**************************************************************************************************************************");
        System.out.println("                                           HANDBALL SIMULATOR 2018");
        System.out.println("                                                    MENU         ");
        System.out.println("**************************************************************************************************************************");
    }

    public void printMenu(){

        System.out.println("1 - ENTRENADOR ENVIA MISSATGE A JUGADORS DE PISTA");
        System.out.println("2 - ARBITRE AMONESTA A JUGADOR X");
        System.out.println("3 - JUGADOR X AGAFA EL ROL Y");
        System.out.println("0 - SORTIR");
        System.out.println("99 - INFORMACIO DEL SIMULADOR");

    }


}
