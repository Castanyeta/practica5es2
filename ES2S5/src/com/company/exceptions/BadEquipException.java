package com.company.exceptions;

public class BadEquipException extends Throwable {
    public BadEquipException(String s) {
        super(s);
    }
}
