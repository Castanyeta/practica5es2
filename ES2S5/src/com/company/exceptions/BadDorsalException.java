package com.company.exceptions;

public class BadDorsalException extends Throwable {
    public BadDorsalException(String s) {
        super(s);
    }
}
