package com.company;
import com.company.simulator.Partit;
import com.company.simulator.ProgramDataSource;

import java.io.IOException;

public class Handbol {

    private static Partit partit;

    public static void main(String[] args) {

        String file = "data.json";


        ProgramDataSource dataSource = new ProgramDataSource();

        try{
            // Reading and Inserting, will create partit
            dataSource.appendJson(file);
            partit = dataSource.getPartit();
            System.out.println("**************************************************************************************************************************");
            System.out.println("The file " + file + " has been imported correctly ");
            System.out.println("**************************************************************************************************************************");
            // Starting simulator!
            partit.start();
        }
        catch (IOException io){
            System.out.println("El fitxer no existeix o no tinc permissos per obrir-lo...");
        }
        catch (org.json.simple.parser.ParseException e) {
            System.out.println("El fitxer Json està mal format i no s'ha pogut importar... ");
        }
        catch (Exception ex){
            System.out.println("Quelcom ha anat malament, contacti amb els irresponsables que han fet el programa...");
        }


    }

}
