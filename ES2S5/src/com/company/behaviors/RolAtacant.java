package com.company.behaviors;

public class RolAtacant implements RolBehavior {

    @Override
    public void transita() {
        System.out.println("transició atacant per defecte");
    }

    @Override
    public void tira() {
        System.out.println("tir atacant per defecte");
    }

    @Override
    public void penetra() {
        System.out.println("penetració atacant per defecte");
    }

    @Override
    public void bloqueja() {
        System.out.println("bloqueig atacant per defecte");
    }

    @Override
    public void pressiona() {
        System.out.println("pressió atacant per defecte");
    }

    @Override
    public String toString() {
        return "(Atacant)";
    }
}
