package com.company.behaviors;

public class Pivot extends RolAtacant {
    public static final int PIVOT                        = 14;
    @Override
    public String toString() {
        return "[ " + PIVOT + " <- " + "Pivot " + super.toString() + " ]";
    }
}
