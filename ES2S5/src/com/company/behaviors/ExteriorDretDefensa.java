package com.company.behaviors;

public class ExteriorDretDefensa extends RolDefensa {

    public static final int EXTERIOR_DRET_DEFENSA        = 7;
    @Override
    public String toString() {
        return  "[ " + EXTERIOR_DRET_DEFENSA + " <- " + "Exterior Dret " + super.toString() + " ]";
    }
}
