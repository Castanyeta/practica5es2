package com.company.behaviors;


/**
 *
 */
public interface RolBehavior {


    void transita();
    void tira();
    void penetra();
    void bloqueja();
    void pressiona();

}
