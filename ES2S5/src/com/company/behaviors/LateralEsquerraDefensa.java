package com.company.behaviors;

public class LateralEsquerraDefensa extends RolDefensa {
    public static final int LATERAL_ESQUERRA_DEFENSA     = 13;
    @Override
    public String toString() {
        return "[ " + LATERAL_ESQUERRA_DEFENSA + " <- " + "Lateral Esquerra " + super.toString() + " ]";
    }
}
