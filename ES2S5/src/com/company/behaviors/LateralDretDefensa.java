package com.company.behaviors;

public class LateralDretDefensa extends RolDefensa {

    public static final int LATERAL_DRET_DEFENSA         = 11;
    @Override
    public String toString() {
        return "[ " + LATERAL_DRET_DEFENSA + " <-> " + "Lateral Dret " + super.toString() + " ]";
    }
}
