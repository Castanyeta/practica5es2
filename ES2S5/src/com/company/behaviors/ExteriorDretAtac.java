package com.company.behaviors;

public class ExteriorDretAtac extends RolAtacant {
    public static final int EXTERIOR_DRET_ATAC           = 6;
    @Override
    public String toString() {
        return "[ " +  EXTERIOR_DRET_ATAC + " <- " + "Exterior Dret " + super.toString() + " ]";
    }
}
