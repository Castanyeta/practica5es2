package com.company.behaviors;

public class LateralDretAtac extends RolAtacant {
    public static final int LATERAL_DRET_ATAC            = 10;
    @Override
    public String toString() {
        return "[ " + LATERAL_DRET_ATAC + " <- " + "Lateral Dret " + super.toString() + " ]";
    }
}
