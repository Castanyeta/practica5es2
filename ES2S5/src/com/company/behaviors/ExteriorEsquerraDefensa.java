package com.company.behaviors;

public class ExteriorEsquerraDefensa extends RolDefensa {

    public static final int EXTERIOR_ESQUERRA_DEFENSA    = 9;
    @Override
    public String toString() {
        return  "[ " + EXTERIOR_ESQUERRA_DEFENSA+ " <- " + "Exterior Esquerra " + super.toString() + " ]";
    }
}
