package com.company.behaviors;

public class CentralDret extends RolDefensa {

    public static final int CENTRAL_DRET                 = 4;

    @Override
    public String toString() {
        return "[ " + CENTRAL_DRET + " <- " + "Central " + super.toString() + " ]";
    }
}
