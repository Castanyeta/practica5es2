package com.company.behaviors;

public class LateralEsquerraAtac extends RolAtacant {
    public static final int LATERAL_ESQUERRA_ATAC        = 12;
    @Override
    public String toString() {
        return "[ " + LATERAL_ESQUERRA_ATAC + " <- " + "Lateral Esquerra " + super.toString() + " ]";
    }
}
