package com.company.behaviors;

public class Avancat extends RolDefensa {

    public static final int AVANCAT = 1;

    @Override
    public String toString() {
        return "[ " + AVANCAT + " <- " + "Avançat " + super.toString() + " ]";
    }
}
