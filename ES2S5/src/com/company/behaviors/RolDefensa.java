package com.company.behaviors;

public class RolDefensa implements RolBehavior {


    @Override
    public void transita() {
        System.out.println("transició defensiva per defecte");
    }

    @Override
    public void tira() {
        System.out.println("tir defensiu per defecte");
    }

    @Override
    public void penetra() {
        System.out.println("penetració defensiva per defecte");
    }

    @Override
    public void bloqueja() {
        System.out.println("bloqueig defensiu per defecte");
    }

    @Override
    public void pressiona() {
        System.out.println("pressió defensiva per defecte");
    }

    @Override
    public String toString() {
        return "(Defensiu)";
    }
}
