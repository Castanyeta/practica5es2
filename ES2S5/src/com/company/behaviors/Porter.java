package com.company.behaviors;

public class Porter extends RolDefensa {
    public static final int PORTER                       = 15;
    @Override
    public String toString() {
        return "[ " + PORTER + " <- " + "Porter ]";
    }

}
