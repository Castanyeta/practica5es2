package com.company.behaviors;

public class CentralDefensor extends RolDefensa {

    public static final int CENTRAL_DEFENSOR             = 3;
    @Override
    public String toString() {
        return "[ " + CENTRAL_DEFENSOR + " <- " + "Central " + super.toString() + " ]";
    }
}
