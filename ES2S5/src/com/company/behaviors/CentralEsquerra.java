package com.company.behaviors;

public class CentralEsquerra extends RolDefensa {

    public static final int CENTRAL_ESQUERRA             = 5;
    @Override
    public String toString() {
        return  "[ " + CENTRAL_ESQUERRA + " <- " + "Central Esquerra " + super.toString() + " ]";
    }
}
