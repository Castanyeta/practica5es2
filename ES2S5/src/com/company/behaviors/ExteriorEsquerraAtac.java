package com.company.behaviors;

public class ExteriorEsquerraAtac extends RolAtacant {

    public static final int EXTERIOR_ESQUERRA_ATAC       = 8;
    @Override
    public String toString() {
        return  "[ " + EXTERIOR_ESQUERRA_ATAC + " <- " + "Exterior Esquerra " + super.toString() + " ]";
    }

}
