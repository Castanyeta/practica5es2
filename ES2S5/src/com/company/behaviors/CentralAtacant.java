package com.company.behaviors;

public class CentralAtacant extends RolAtacant {

    public static final int CENTRAL_ATACANT = 2;

    @Override
    public String toString() {

        return "[ " + CENTRAL_ATACANT + " <- " + "Central " + super.toString() + " ]";
    }
}

