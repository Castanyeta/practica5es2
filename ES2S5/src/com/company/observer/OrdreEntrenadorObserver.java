package com.company.observer;

public interface OrdreEntrenadorObserver {

    // S'actualitza a partir de l'ordre rebuda
    void update(String ordre);

}
