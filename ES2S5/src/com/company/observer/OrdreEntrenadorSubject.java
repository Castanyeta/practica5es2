package com.company.observer;

public interface OrdreEntrenadorSubject {
    // Registre de jugadors Observers
    void registerObserver(OrdreEntrenadorObserver o, boolean enJoc);
    // Borra un jugador Observer
    void removeObserver(OrdreEntrenadorObserver o);
    // Notifica una ordre als observers
    void notify(String ordre, Boolean enJoc);

}
