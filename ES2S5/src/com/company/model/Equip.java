package com.company.model;

import com.company.exceptions.BadDorsalException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.*;

public class Equip {

    /** Constants */
    private static final String ID_EQUIP = "idEquip";
    private static final String NOM = "nomEquip";
    private static final String ENTRENADOR = "entrenador";
    private static final String JUGADORS = "integrants";

    /** Atributs */
    protected String idEquip; // Identificador de l'Equip
    protected String nomEquip; // Nom complert de l'equip
    protected Map<Integer,Jugador> integrants; // Llista de Membres del club
    protected Entrenador entrenador; // Entrenador de l'equip


    /**
     * Constructor d'Equip
     * @pre: true
     * @post: Equip creat
     * @param id, identificador de l'Equip (3 lletres)
     * @param nomEquip, nom complert de l'equip
     */
    public Equip(String id,String nomEquip){
        this.idEquip = id;
        this.nomEquip = nomEquip;
        this.integrants = new HashMap<>();
    }

    /**
     * Constructor que crida al this
     * @param json objecte amb el que es crearà el l'equip i tots els seus components
     */
    public Equip(JSONObject json){
        this((String) json.get(ID_EQUIP), (String) json.get(NOM));
        this.entrenador = new Entrenador((JSONObject) json.get(ENTRENADOR));
        JSONArray jugadors = (JSONArray) json.get(JUGADORS);
        for(Object obj : jugadors){
            Jugador j = new Jugador((JSONObject) obj);
            integrants.put(j.getDorsal(), j);
        }
    }


    /**
     * @pre True
     * @return una collecció de jugador de l'equip
     */
    public Collection<Jugador> getJugadors(){
        return integrants.values();
    }

    /**
     * PRE : true
     * @param dorsal Identificador del jugador donat un equip
     * @return Cert si el dorsal és a l'equip, falç altrament
     */
    public boolean conteJugador(int dorsal){
        return integrants.containsKey(dorsal);
    }

    /**
     * PRE : True
     * Post : Ordre enviada a jugadors
     * @param ordre Missatge a enviar als jugadors
     * @param aPista (true -> pista, false -> banqueta, null -> tots).
     */
    public void entrenadorOrdena(String ordre, Boolean aPista){
        entrenador.notify(ordre, aPista);
    }

    /**
     * Retorna l'entrenador de l'equip
     * @return entrenador;
     */
    public Entrenador getEntrenador(){
        return  this.entrenador;
    }

    /**
     * PRE : true
     * @param dorsal, dorsal per identificar el jugador
     * @throws BadDorsalException Dorsal de jugador no existeix
     * @return el jugador identificat per el dorsal passat com a paràmetre
     */
    public Jugador getJugador(int dorsal) throws BadDorsalException {
        if(!integrants.containsKey(dorsal))
            throw new BadDorsalException("No existeix el jugador amb el dorsal " + dorsal + " a l'equip " + nomEquip);
        return integrants.get(dorsal);
    }

    /**
     * @pre true
     * @return Jugadors de pista
     */
    public Collection getPista() {
        List pista = new ArrayList();
        for(Jugador j : integrants.values()){
            if(j.isEnJoc()){
                pista.add(j);
            }
        }
        return pista;
    }

    @Override
    public int hashCode() {
        return idEquip.hashCode();
    }

    @Override
    public String toString() {


        StringBuilder builder = new StringBuilder();
        builder.append("--------------------------------------------------------------------------------------------------------------------------\n");
        builder.append("Equip : { id : " + idEquip + ", nom : " + nomEquip + "\n");
        builder.append("Entrenador : " + entrenador + "\n");
        builder.append("Jugadors : \n");
        for(Jugador j : integrants.values()){
            builder.append("  " + j + "\n");
        }
        builder.append("--------------------------------------------------------------------------------------------------------------------------");
        return builder.toString();
    }


}
