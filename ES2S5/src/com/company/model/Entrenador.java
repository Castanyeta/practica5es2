package com.company.model;
import com.company.observer.OrdreEntrenadorObserver;
import com.company.observer.OrdreEntrenadorSubject;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 *
 */
public class Entrenador extends MembreDeClub implements OrdreEntrenadorSubject {


    /** Constants */
    private static final String NOM = "nom";
    private static final String FITXA = "fitxa";

    /** Atributs */
    private List<OrdreEntrenadorObserver> jugadorsBanquetaObservers; //Llista de jugadors que poden rebre ordres que estan a la banqueta
    private List<OrdreEntrenadorObserver> jugadorsDeCampObservers; //Llista de jugadors que poden rebre ordres que estan al camp

    /**
     * Constructor Entrenador
     * @param fitxa, num de fitxa que identifica l'entrenador
     * @param nom, nom de l'entrenador
     */
    public Entrenador(String fitxa, String nom){
        this.nom = nom;
        this.numeroFitxa = fitxa;
        this.jugadorsBanquetaObservers = new ArrayList<OrdreEntrenadorObserver>();
        this.jugadorsDeCampObservers = new ArrayList<OrdreEntrenadorObserver>();
    }

    /**
     * Crida al constructor this
     * @param json Objecte amb el que es crearà this
     */
    public Entrenador(JSONObject json) {
        this((String) json.get(FITXA), (String) json.get(NOM));
    }

    /**
     * Registre d'observadors de la classe Subject, que en aquest cas tenim com Observadors -> Jugadors, i Subject -> Entrenador
     * @param o, Observer d'entrenador que volem registrar.
     * @param enJoc, Booleà que ens diu si un observer està en joc o no.
     */
    @Override
    public void registerObserver(OrdreEntrenadorObserver o, boolean enJoc) {
        if(enJoc){
            jugadorsDeCampObservers.add(o);
        }else{
            jugadorsBanquetaObservers.add(o);
        }
    }

    /**
     * PRE : True
     * @param list Llista d'observers
     * @param o Observer a eliminar de list
     * @return true -> o eliminat de list, false -> o no existeix a list
     */
    private boolean removeFromListObserver(List<OrdreEntrenadorObserver> list, OrdreEntrenadorObserver o){
        Iterator<OrdreEntrenadorObserver> jugadors = list.iterator();
        boolean trobat = false;
        while(jugadors.hasNext() && !trobat){
            OrdreEntrenadorObserver j = jugadors.next();
            if(o.equals(j)){
                list.remove(j);
                trobat=true;
            }
        }
        return trobat;
    }

    /**
     * PRE : True
     * POST : o eliminat d'observador
     * @param o Observer a eliminar.
     */
    @Override
    public void removeObserver(OrdreEntrenadorObserver o) {
        if(!removeFromListObserver(jugadorsBanquetaObservers,o))
            removeFromListObserver(jugadorsDeCampObservers,o);
    }

    /**
     * PRE : True
     * POST : ordre enviada a tots els elements de list.
     * @param list Llistat d'observers que reben l'ordre
     * @param ordre Missatge que s'envia a list
     */
    private void notifyListObserver(List<OrdreEntrenadorObserver> list, String ordre){
        Iterator<OrdreEntrenadorObserver> jugadors = list.iterator();
        while(jugadors.hasNext()){
            jugadors.next().update(ordre);
        }
    }

    /**
     * @pre true
     * @post ordre enviada a tots els jugadors en funció de enjoc
     * @param ordre missatge a enviar
     * @param enJoc true -> jugadors de pista, false -> banqueta, null -> tothom
     */
    @Override
    public void notify(String ordre, Boolean enJoc) {
        if(enJoc == null){
            notifyListObserver(jugadorsDeCampObservers, ordre);
            notifyListObserver(jugadorsBanquetaObservers, ordre);
        }
        else if(enJoc){
            notifyListObserver(jugadorsDeCampObservers, ordre);
        }else{
            notifyListObserver(jugadorsBanquetaObservers, ordre);
        }
    }

    @Override
    public String toString() {
        return "{" + nom + ", " + numeroFitxa + "}";
    }


}
