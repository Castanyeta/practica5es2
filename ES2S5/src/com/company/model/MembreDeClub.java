package com.company.model;

import com.company.observer.OrdreEntrenadorObserver;
import com.company.observer.OrdreEntrenadorSubject;


/**
 * Classe abstracte que recull les funcionalitats generals de membres d'un equip
 */
public abstract class MembreDeClub implements OrdreEntrenadorObserver {


    /** Atributs */
    protected OrdreEntrenadorSubject ordreEntrenadorSubject; // Subjecte que ens envia ordres (Entrenador)
    protected String numeroFitxa; // Identificador inequívoc de membre de club
    protected String nom; // nom del membre de club
    protected int grogues; // Nombre de targetes grogues acumolades
    protected boolean expulsat; // Si està o no expulsat
    protected int exclusions; // Nombre d'exclusions acumolades (Desconeixem si al handbol és important, ho hem posat per si de cas)
    protected String ultimaOrdre;

    /**
     * PRE : true
     * POST : e afegit com a subject de this
     * @param e Subject que realtizarà ordres
     */
    public void setEntrenador(Entrenador e){
        ordreEntrenadorSubject = e;
    }

    public void addGroga(){
        grogues++;
    }

    public void setExpulsat(){
        expulsat = true;
    }

    public void addExclusio(){
        exclusions++;
    }

    public int getGrogues(){
        return grogues;
    }

    public boolean isExpulsat(){
        return expulsat;
    }

    /**
     * PRE : True
     * POST : ordre assignada com a ultima ordre rebuda
     * @param ordre rebuda
     */
    public void update(String ordre){
        ultimaOrdre = ordre;
    }

    @Override
    public int hashCode() {
        return numeroFitxa.hashCode();
    }

    @Override
    public String toString() {
        return  nom + ", Num Fitxa : " + numeroFitxa + ",\n " +
                "       Grogues: " + grogues + ", Exclusions : " + exclusions + ", EXPULSAT : " + expulsat
                + (ultimaOrdre != null ? ",\n        Ultima Ordre : " + ultimaOrdre: "" );
    }

}
