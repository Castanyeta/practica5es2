package com.company.model;

import com.company.exceptions.BadSancioException;
import com.company.simulator.Partit;
import org.json.simple.JSONObject;


public class Arbitre {


    /** Constants */
    private static final String NOM_JSON = "nom";
    private static final String COLLEGIAT_JSON = "numColegiat";

    /** Atributs */
    private String nom;
    private String colegiat;

    /**
     * Constructor this amb json
     * @param json objecte per construir this
     */
    public Arbitre(JSONObject json){
        this((String) json.get(NOM_JSON), (String) json.get(COLLEGIAT_JSON));
    }

    /**
     * Constructor
     * @param nom de l'àrbitre
     * @param colegiat numero de col·legiat de l'àrtre
     */
    public Arbitre(String nom, String colegiat){
        this.nom = nom;
        this.colegiat = colegiat;
    }

    /**
     * @pre True
     * @post sancionable sancionat amb tipus
     * @param tipus de sanció que s'aplica
     * @param sancionable Membre de club que es sancionarà
     * @throws BadSancioException Quan la sanció no existeix o no es pot crear
     */
    public void sanciona(String tipus, MembreDeClub sancionable) throws BadSancioException  {

        if(tipus.equals(Partit.GROGA)){
            sancionable.addGroga();
            int grogues = sancionable.getGrogues();

            if(grogues == 2 || grogues == 3){
                sanciona(Partit.EXCLUSIO, sancionable);
            }
            else if(grogues == 4){
                sanciona(Partit.VERMELLA, sancionable);
            }
        }
        else if(tipus.equals(Partit.EXCLUSIO)){
            sancionable.addExclusio();
        }
        else if(tipus.equals(Partit.VERMELLA)){
            sancionable.setExpulsat();
        }
        else{
            throw new BadSancioException("No existeix un tipus de sanció " + tipus);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(!(obj instanceof Arbitre)) return false;
        else{
            return colegiat.equals(((Arbitre) obj).colegiat);
        }
    }

    @Override
    public int hashCode() {
        return colegiat.hashCode();
    }

    @Override
    public String toString() {
        return "nom : " + nom + ", num col·legiat : " + colegiat;
    }
}
