package com.company.model;

import com.company.behaviors.RolBehavior;
import com.company.factories.RolFactory;
import org.json.simple.JSONObject;

/**
 * Classe Jugador
 */
public class Jugador extends MembreDeClub {

    /** Constants */
    private static final String NOM = "nom";
    private static final String FITXA = "fitxa";
    private static final String DORSAL = "dorsal";
    private static final String ROL = "rol";
    private static final String ENJOC = "enJoc";

    /** Atributs */
    private int dorsal; // Identificador d'un jugador donat un partit i un club.
    private RolBehavior comportament; // Rol del jugador
    private boolean enJoc; // Conté boleà que indica si està o no a la pista

    /**
     * Constructor de Jugador
     * @param numFitxa identificador del jugador
     * @param nom del jugador
     * @param dorsal del jugador
     * @param comportament rol que adquireix només començar  (CONSTANT RolFactory)
     * @param enJoc si està o no en pista
     */
    public Jugador(String numFitxa, String nom, int dorsal, RolBehavior comportament, boolean enJoc) {
        this.numeroFitxa = numFitxa;
        this.nom = nom;
        this.dorsal = dorsal;
        this.comportament = comportament;
        this.enJoc = enJoc;
    }

    /**
     * Constructor que crida al this
     * @param json objecte json amb el que es construeix el jugador.
     */
    public Jugador(JSONObject json) {
        this(
                (String) json.get(FITXA),
                (String) json.get(NOM),
                ((Long) json.get(DORSAL)).intValue(),
                (new RolFactory().crearRol(((Long) json.get(ROL)).intValue())),
                ((boolean) json.get(ENJOC))
        );
    }

    /**
     * PRE : True
     * @return retorna el dorsal del jugador.
     */
    public int getDorsal() {
        return dorsal;
    }


    /**
     * Ens serveix per assignar el rol a un jugador
     * @param nourol, nou rol que es vol assignar al jugador
     */
    public void setRol(RolBehavior nourol) {
        comportament = nourol;
    }

    /**
     * Pre : True
     * @return Si està o no en pista
     */
    public boolean isEnJoc(){
        return enJoc && !this.expulsat;
    }

    @Override
    public int hashCode() {
        return dorsal;
    }

    @Override
    public String toString() {
        return "---------------------------------------\n" + dorsal + " -> " + super.toString() + ",\n        Rol : " + comportament + "\n        " + (isEnJoc() ? "ESTÀ JUGANT" : "NO ESTÀ JUGANT") ;
    }

}


